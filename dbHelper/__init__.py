import mysql.connector
from mysql.connector import Error

query_create_table = 'CREATE TABLE IF NOT EXISTS `parsed_logs`' \
                     '(`id` int(11) NOT NULL AUTO_INCREMENT,' \
                     '`ip` varchar(15) NOT NULL,' \
                     '`datetime` datetime NOT NULL,' \
                     '`url` text NOT NULL,' \
                     'PRIMARY KEY (`id`)) ' \
                     'ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci'


class DbHelper:
    def __init__(self, user, password, database):
        self.user = user
        self.password = password
        self.database = database

    def connect(self):
        try:
            conn = mysql.connector.connect(user=self.user, password=self.password,
                                           host='127.0.0.1', database=self.database)
        except Error as e:
            print(e)
        return conn

    def create_table(self):
        conn = self.connect()
        if conn.is_connected():
            try:
                cursor = conn.cursor()
                cursor.execute(query_create_table)
                conn.commit()
                cursor.close()
                conn.close()
            except Error:
                print(Error)

    def select_parsed_logs_is_empty(self):
        conn = self.connect()
        if conn.is_connected():
            cursor = conn.cursor()
            q = 'SELECT * FROM parsed_logs'
            cursor.execute(q)
            rows = cursor.fetchall()
            conn.close()
            if len(rows) == 0:
                return True
            else:
                return False
        else:
            return False

    def insert_logs(self, parsed):
        conn = self.connect()
        self.create_table()
        if conn.is_connected() and self.select_parsed_logs_is_empty():
            cursor = conn.cursor()
            q = "INSERT INTO parsed_logs(ip, datetime, url) VALUES (%s, %s, %s)"
            for i in parsed:
                try:
                    cursor.execute(q, (i.ip, i.datetime, i.url))
                except Error as e:
                    print(e)
            conn.commit()
            cursor.close()
            conn.close()
            if not self.select_parsed_logs():
                return True
            else:
                return False
        else:
            return False

    def select_parsed_logs(self):
        conn = self.connect()
        if conn.is_connected():
            cursor = conn.cursor()
            q = 'SELECT * FROM parsed_logs'
            cursor.execute(q)
            rows = cursor.fetchall()
            conn.close()
            return rows
