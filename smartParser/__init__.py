
class ParsedLine:
    def __init__(self, ip, datetime, url):
        self.ip = ip
        self.datetime = datetime
        self.url = url


class SmartParser:
    def __init__(self, file_path):
        self.file_path = file_path
        self.parsed = []

    def parse(self):
        try:
            with open(self.file_path, 'r', encoding='utf-8') as f:
                for line in f.readlines():
                    ip = line.split()[6]
                    datetime = '{} {}'.format(line.split()[2], line.split()[3])
                    url = line.split()[7]
                    self.parsed.append(ParsedLine(ip, datetime, url))
        except IndexError:
            print('index in you list does note exist')
        except FileNotFoundError:
            print('Check you file path please!')
        return self.parsed
