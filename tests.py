import unittest
from smartParser import SmartParser
from dbHelper import DbHelper
from smartAnalytics import found_country


class TestParser(unittest.TestCase):
    def test_format(self):
        smart_parser = SmartParser('data/logs.txt')
        parsed = smart_parser.parse()
        self.assertEqual(parsed[0].ip, '121.165.118.201', 'Error in parse ip address')
        self.assertEqual(parsed[0].datetime, '2018-08-01 00:01:35', 'Error in parse datetime')
        self.assertEqual(parsed[0].url, 'https://all_to_the_bottom.com/', 'Error in parse url')


class TestDbHelper(unittest.TestCase):
    def test_success_conn(self):
        helper = DbHelper('root', 'root200', 'logs')
        conn = helper.connect()
        self.assertTrue(conn.is_connected(), 'Invalid parameters for connect')
        conn.close()


class TestAnalytics(unittest.TestCase):
    def test_found_country(self):
        country = found_country('72.229.28.185')
        self.assertEqual(country, 'United States')

    def test_nonexistent_ip(self):
        country = found_country('10.221.29.184')
        self.assertIsNone(country)
    


if __name__ == '__main__':
    unittest.main()
