from geolite2 import geolite2
import operator
from urllib import parse


def found_country(ip):
    rd = geolite2.reader()
    try:
        dict_country = rd.get(ip)
        country = dict_country['country']['names']['en']
        return country
    except TypeError:
        return None
    except KeyError:
        return dict_country['registered_country']['names']['en']


def popular_country(ip_list):
    country_dict = dict()
    for i in ip_list:
        try:
            country_dict[i] = country_dict[i] + 1
        except KeyError:
            country_dict[i] = 1
    return max(country_dict.items(), key=operator.itemgetter(1))[0]


class SmartAnalytics:
    def __init__(self, logs):
        self.logs = logs
        self.ip_list = []
        self.number_inquiry = dict()
        self.ip_list_fresh_fish = []
        for i in self.logs:
            country = found_country(i[1])
            self.ip_list.append(country)
            if i[3].find('fresh_fish/') != -1 and country is not None:
                self.ip_list_fresh_fish.append(country)
            try:
                self.number_inquiry[i[2].strftime('%j:%H')] += 1
            except KeyError:
                self.number_inquiry[i[2].strftime('%j:%H')] = 1

    def the_most_popular_country(self):
        return 'The most popular country is {}'.format(popular_country(self.ip_list))

    def the_most_popular_country_fresh_fish(self):
        return 'The most popular country who interested fresh fish is {} '.format(
            popular_country(self.ip_list_fresh_fish))

    def max_number_inquire_in_hour(self):
        return 'Max number query in hour - {}'.format(max(self.number_inquiry.items(), key=operator.itemgetter(1))[1])

    def return_users(self):
        count = 0
        number_of_purchases = dict()
        for i in self.logs:
            try:
                if i[3].find('success_pay') != -1:
                    number_of_purchases[i[1]] += 1
            except KeyError:
                number_of_purchases[i[1]] = 1

        for i in number_of_purchases.values():
            if i > 1:
                count += 1
        return 'Return users - {}'.format(count)

    def abandoned_baskets(self):
        set_param = set()
        pay_list = []
        count = 0
        for i in self.logs:
            if i[3].find('cart?') != -1:
                query_cart_id = parse.parse_qs(parse.urlparse(i[3]).query)['cart_id'][0]
                set_param.add(query_cart_id)
            if i[3].find('pay?') != -1:
                query_cart_id = parse.parse_qs(parse.urlparse(i[3]).query)['cart_id'][0]
                pay_list.append(query_cart_id)
        for i in set_param:
            if i not in pay_list:
                count += 1
        return 'Number of abandoned baskets - {}'.format(count)

    def times_of_day_frozen_fish(self):
        times_dt = {'night': 0, 'afternoon': 0, 'evening': 0, 'morning': 0}
        for i in self.logs:
            if i[3].find('frozen_fish') != -1:
                if 0 <= i[2].time().hour <= 6 and 0 <= i[2].time().minute <= 59:
                    times_dt['night'] += 1
                if 6 <= i[2].time().hour <= 12 and 0 <= i[2].time().minute <= 59:
                    times_dt['morning'] += 1
                if 12 <= i[2].time().hour <= 18 and 0 <= i[2].time().minute <= 59:
                    times_dt['afternoon'] += 1
                if 18 <= i[2].time().hour <= 0 <= i[2].time().minute <= 59:
                    times_dt['evening'] += 1
        return 'The time of day when the most commonly viewed frozen fish is {}'.format(
            max(times_dt.items(), key=operator.itemgetter(1))[0])

    def goods_by_with_semi_manufactures(self):
        semi_list = []
        frozen_list = []
        fresh_list = []
        caviar_list = []
        canned_list = []
        for i in self.logs:
            if i[3].find('cart?') != -1:
                query_goods_id = int(parse.parse_qs(parse.urlparse(i[3]).query)['goods_id'][0])
                query_cart_id = parse.parse_qs(parse.urlparse(i[3]).query)['cart_id'][0]
                if 14 <= query_goods_id <= 18:
                    semi_list.append(query_cart_id)
                if 1 <= query_goods_id <= 7:
                    fresh_list.append(query_cart_id)
                if 8 <= query_goods_id <= 13:
                    frozen_list.append(query_cart_id)
                if 19 <= query_goods_id <= 21:
                    canned_list.append(query_cart_id)
                if 22 <= query_goods_id <= 24:
                    caviar_list.append(query_cart_id)
        dt = {'frozen_fish': 0, 'fresh fish': 0, 'canned food': 0, 'caviar': 0, 'frozen fish': 0}
        for i in semi_list:
            if i in frozen_list:
                dt['frozen fish'] += 1
            if i in fresh_list:
                dt['fresh fish'] += 1
            if i in caviar_list:
                dt['caviar'] += 1
            if i in canned_list:
                dt['canned food'] += 1
        return 'Category goods buy with semi manufactures is {}'.format(max(dt.items(), key=operator.itemgetter(1))[0])
