from smartParser import SmartParser
from dbHelper import DbHelper
from smartAnalytics import SmartAnalytics

if __name__ == '__main__':
    smart_parser = SmartParser('data/logs.txt')
    helper = DbHelper('root', 'root200', 'logs')
    parsed = smart_parser.parse()
    helper.insert_logs(parsed)
    logs = helper.select_parsed_logs()
    analytics = SmartAnalytics(logs)
    print(analytics.the_most_popular_country())
    print(analytics.the_most_popular_country_fresh_fish())
    print(analytics.times_of_day_frozen_fish())
    print(analytics.max_number_inquire_in_hour())
    print(analytics.goods_by_with_semi_manufactures())
    print(analytics.abandoned_baskets())
    print(analytics.return_users())
